const {
  browser,
  $,
  by
} = require("protractor");
const EC = protractor.ExpectedConditions;

class MosaicAdministrationPage {
  constructor() {
    // New User Dialog elements
    // /platform-admin page
    this.newUserButton = element(by.buttonText("New user"));
    this.firstNameInput = element(by.id("mat-input-0"));
    this.lastNameInput = element(by.id("mat-input-1"));
    this.emailInput = element(by.id("mat-input-2"));
    this.passwordInput = element(by.id("mat-input-4"));
    this.confirmPasswordInput = element(by.name("msc-pass-confirm"));
    this.telephoneInput = element(by.id("mat-input-3"));
    this.nextButton = element(by.css(".mat-flat-button"));
    this.companySelect = element(by.id("mat-select-1"));
    this.autoTestOption =element(by.cssContainingText("mat-option-text", "autoTest"));
    // "Cancel"
    // "Previous"
    this.createUserButton = element(by.buttonText("Create user"));
  }

  async openPlatformAdministration() {
    browser.open(`${browser.baseUrl}/platform-administration`);
  }

  async clickNewUserButton() {
    browser.wait(EC.elementToBeClickable(this.newUserButton, 3000));
    this.newUserButton.click();
  }

  async createNewUser() {
    browser.wait(EC.elementToBeClickable(this.firstNameInput, 3000));
    this.firstNameInput.sendKeys("Fred");
    browser.wait(EC.elementToBeClickable(this.lastNameInput, 3000));
    this.lastNameInput.sendKeys("Flintstone");
    browser.wait(EC.elementToBeClickable(this.emailInput, 3000));
    this.emailInput.sendKeys(
      `sweetbillybangkok+${Math.floor(Date.now() / 1000)}@gmail.com`
    );
    this.companySelect.click();
    browser.wait(EC.elementToBeClickable(this.autoTestOption, 3000));

    this.passwordInput.sendKeys("FX9Umws4LqTT9Qr!");
    this.confirmPasswordInput.sendKeys("FX9Umws4LqTT9Qr!");
    this.telephoneInput.sendKeys("4155551212");
    browser.wait(EC.elementToBeClickable(this.firstNameInput, 3000));
    this.nextButton.click();
    this.createUserButton.click();
    browser.driver.sleep(3000);
  }
}

export default new MosaicAdministrationPage();