import { browser } from "protractor";
import { protractor } from "protractor/built/ptor";
const EC = protractor.ExpectedConditions;

class MosaicLoginPage {
  constructor() {
    this.welcomeText = element(by.css("h1.c-mb-2.login-header"));
    this.userInput = element(by.id("mat-input-0"));
    this.passwordInput = element(by.id("mat-input-1"));
    this.loginButton = element(
      by.css(
        "button.mat-focus-indicator.w-100.c-mb-1.msc-button.mat-flat-button.mat-button-base.mat-primary"
      )
    );
    this.forgotPassword = element(by.css(".mat-button > .mat-button-wrapper"));

    this.dangerText = element(by.css("p.text-danger"));
    this.toastMessage = element(by.css(".toast-message"))
  }

  async open() {
    await browser.get(browser.baseUrl);
  }

  async title() {
    return await browser.getTitle();
  }

  async getWelcomeText() {
    browser.wait(EC.visibilityOf(this.welcomeText, 5000));
    return await this.welcomeText.getText();
  }

  async getDangerText() {
    browser.wait(EC.visibilityOf(this.dangerText, 5000));
    return await this.dangerText.getText();
  }

  async getToastMessage() {
    browser.wait(EC.visibilityOf(this.toastMessage, 5000));
    return await this.toastMessage.getText();
  }

  async loginAs(username, password) {
    browser.wait(EC.elementToBeClickable(this.userInput, 5000));
    await this.userInput.sendKeys(username);
    await this.passwordInput.sendKeys(password);
    await this.loginButton.click();
    
  }

  async forgotPasswordAs(username, password) {
      await this.userInput.sendKeys(username);
      await this.passwordInput.sendKeys(password);
      browser.wait(EC.elementToBeClickable(this.forgotPassword, 5000));
      await this.forgotPassword.click();
  }
}
export default new MosaicLoginPage();
