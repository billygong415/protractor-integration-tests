import MosaicLoginPage from "../pages/mosaicloginpage";
import MosaicMenu from "../pages/mosaicmenu";
import { ExpectedConditions, browser } from "protractor";

describe("Verify Mosaic signin", () => {
  beforeEach(async () => {
    await MosaicLoginPage.open();
  });

  it("should have a title", async () => {
    expect(await MosaicLoginPage.title()).toEqual("Welcome to Mosaic");
  });

  it("should display welcome text", async () => {
    expect(await MosaicLoginPage.getWelcomeText()).toEqual("Welcome back");
  });

  it("should not activate the login button with no username and no password", async () => {
    await MosaicLoginPage.loginAs("", "");
    expect(await MosaicLoginPage.loginButton.isEnabled()).toBe(false);
  });

  it("should not activate the login button with no username", async () => {
    await MosaicLoginPage.loginAs("", "aaasdfasdf");
    expect(await MosaicLoginPage.loginButton.isEnabled()).toBe(false);
  });

  it("should not activate the login button with no password", async () => {
    await MosaicLoginPage.loginAs("free2rhyme@yahoo.com", "");
    expect(await MosaicLoginPage.loginButton.isEnabled()).toBe(false);
  });

  it("should not login with a valid username and invalid password", async () => {
    await MosaicLoginPage.loginAs("billy@mosaic.tech", "aaasdfasdf");
    expect(await MosaicLoginPage.getDangerText()).toEqual(
      "Invalid email and password combination"
    );
  });

  it("should provide an error on attempting Forgot Password flow with an invalid password", async () => {
    await MosaicLoginPage.forgotPasswordAs("notbilly@mosaic.tech", "aaasdfasdf");
    expect(await MosaicLoginPage.getToastMessage()).toEqual(
      "Email could not be sent"
    );
  });

});

xdescribe("Verify Companies page", () => {
  it("should login and navigate to the Companies page", async () => {
    await MosaicLoginPage.open();
    await MosaicLoginPage.loginAs("billy@mosaic.tech", "Abc123!!");
    await MosaicMenu.navigateToCompanies();
    browser.driver.sleep(3000); //debug
  });
});

xdescribe("Verify Syncs page", () => {
  it("should login and navigate to the Syncs page", async () => {
    await MosaicLoginPage.open();
    await MosaicLoginPage.loginAs("billy@mosaic.tech", "Abc123!!");
    await MosaicMenu.navigateToSyncs();
    browser.driver.sleep(3000); //debug
  });
});

xdescribe("Verify Jobs page", () => {
  it("should login and navigate to the Jobs page", async () => {
    await MosaicLoginPage.open();
    await MosaicLoginPage.loginAs("billy@mosaic.tech", "Abc123!!");
    await MosaicMenu.navigateToJobs();
    browser.driver.sleep(3000); //debug
  });
});
