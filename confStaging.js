exports.config = {
    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['./specs/*-spec.js'],
    // Options to be passed to Jasmine-node.
    jasmineNodeOpts: {
        showColors: true, // Use colors in the command line report.
    },
    baseUrl: 'https://app-staging.mosaic.tech',
    credentials: {
        username: process.env.PROD_USER,
        password: process.env.PROD_PWORD
    },
    onPrepare: () => {
        require("@babel/register");
        browser.ignoreSynchronization = true;
    }
};