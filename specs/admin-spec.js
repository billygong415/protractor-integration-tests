import MosaicLoginPage from "../pages/mosaicloginpage";
import MosaicAdministrationPage from "../pages/mosaicadministrationpage";
import MosaicMenu from "../pages/mosaicmenu";
import { ExpectedConditions, browser } from "protractor";

xdescribe("Verify Administration page", () => {
  beforeEach(async () => {
    await MosaicLoginPage.open();
    await MosaicLoginPage.loginAs("billy@mosaic.tech", "Abc123!!");
  });

  // afterEach(async () => {
  //   await MosaicMenu.navigatetoSignOut();
  // });

  it("should login and navigate to the Administration page ", async () => {
    await MosaicMenu.navigateToAdministration();
    await MosaicAdministrationPage.clickNewUserButton();
    await MosaicAdministrationPage.createNewUser();
    // browser.driver.sleep(3000); //debug
    
  });
});
