exports.config = {
    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['./specs/spec.js'],
    // Options to be passed to Jasmine-node.
    jasmineNodeOpts: {
        showColors: true, // Use colors in the command line report.
    },
    multiCapabilities: [{
        'browserName': 'internet explorer',
        'platform': 'Windows 8.1',
        'version': '11',
        'name': 'Win8.1/IE11...'
    }, {
        'browserName': 'firefox',
        'platform': 'Linux',
        'version': '45',
        'name': 'Linux/Firefox...'
    }],
    sauceUser: process.env.SAUCE_USERNAME,
    sauceKey: process.env.SAUCE_ACCESS_KEY,
    onPrepare: () => {
        require("@babel/register");
        browser.ignoreSynchronization = true;
    }
}