const { browser, $, by } = require("protractor");
const EC = protractor.ExpectedConditions;

class MosaicMenu {
  constructor() {
    this.mosaicIcon = element(
      by.css("mat-icon.mat-icon.notranslate.mat-icon-no-color.ng-star-inserted")
      
    );

    this.userIcon = element(
      by.css(
        ".mat-nav-list:nth-child(2) > .ng-star-inserted:nth-child(3) .msc-list-item"
      )
    );
    
    
    this.companiesIcon = element(
      by.css(
        ".msc-popover-menu-list > .ng-star-inserted:nth-child(1) .mat-list-item-content"
      )
    );

    this.syncsIcon = element(
      by.css(
        ".msc-popover-menu-list > .ng-star-inserted:nth-child(2) .mat-list-item-content"
      )
    );

    this.jobsIcon = element(
      by.css(
        ".msc-popover-menu-list > .ng-star-inserted:nth-child(3) .mat-list-item-content"
      )
    );

    this.platformAdministrationItem = element(
      by.css(
        ".msc-popover-menu-list > .ng-star-inserted:nth-child(4) .mat-list-item-content"
      )
    );
    
    this.signOutItem = element(
      by.css(
        ".msc-popover-menu-list > .ng-star-inserted:nth-child(10) .mat-list-item-content"
      )
    );

  }
  async clickUserIcon() {
    browser.wait(EC.elementToBeClickable(this.userIcon, 5000));
    await this.userIcon.click();
  }
  async isMosaicIconPresent() {
    browser.wait(EC.elementToBeClickable(this.mosaicIcon, 5000));
    return await this.mosaicIcon.isDisplayed();
  }

  async navigateToCompanies() {
    this.clickUserIcon();
    browser.wait(EC.elementToBeClickable(this.companiesIcon, 5000));
    await this.companiesIcon.click();
  }

  async navigateToSyncs() {
    this.clickUserIcon();
    browser.wait(EC.elementToBeClickable(this.syncsIcon, 5000));
    await this.syncsIcon.click();
  }

  async navigateToJobs() {
    this.clickUserIcon();
    browser.wait(EC.elementToBeClickable(this.jobsIcon, 5000));
    await this.jobsIcon.click();
  }

  async navigateToAdministration() {
    this.clickUserIcon();
    browser.wait(
      EC.elementToBeClickable(this.platformAdministrationItem, 5000)
    );
    await this.platformAdministrationItem.click();
  }

  async navigatetoSignOut() {
    this.clickUserIcon();
    browser.wait(EC.elementToBeClickable(this.signOutItem, 5000));
    
  }
}

export default new MosaicMenu();
